import base64
import json
from urllib.parse import urlencode

import pytest

from flask import url_for


def get_id(node):
    return base64.b64decode(node['id']).decode().split(':')[-1]


def url_string(**url_params):
    string = url_for('graphql')
    if url_params:
        string += '?' + urlencode(url_params)
    return string


j = lambda **kwargs: json.dumps(kwargs)


def response_json(response):
    return json.loads(response.data.decode())


class TestGraphQlQuery:
    @pytest.mark.parametrize("query, status_code, count, firt_in_list", [
        ('{cards{pageInfo{startCursor,endCursor,hasNextPage},edges{cursor,node{id,name}}}}', 200, 25, None),  # список карточек
        ('{cards{edges{node{id}}}}', 200, 25, None),  # список карточек
        ('{cards{edges{node{id,carditems{edges{node{id,description}}}}}}}', 200, 25, None),  # список карточек
        ('{cards{edges{node{id,name,dateIn,carditems{edges{node{id,description}}}}}}}', 200, 25, None),  # список карточек с действиями

        ('{cards(first:2,after:"YXJyYXljb25uZWN0aW9uOjA="){edges{node{id,name}}}}', 200, 2, '2'),  # пагинация

        ('{cards(sort:name_asc){edges{node{id,name}}}}', 200, 25, '1'),  # сортировка карточки по названию.
        ('{cards(sort:name_desc){edges{node{id,name}}}}', 200, 25, '30'),  # сортирвка карточки по названию
        ('{cards(sort:items_count_desc, first:1){edges{node{id,name,carditems{edges{node{id,description}}}}}}}', 200, 25, '3'),  # сортировка по количеству действий

        ('{cards(name:"Card 01"){edges{node{id,name,}}}}', 200, 1, '1'),  # фильтрация по названию
        ('{cards(items:2){edges{node{id,name,}}}}', 200, 1, '3'),  # фильтрация колдействий
    ])
    def test_query_cards(self, client, initial_data, query, status_code, count, firt_in_list):
        r = client.get(url_string(query=query))
        assert r.status_code == 200
        result = response_json(r)
        assert result.get('error') is None
        edges_list = result['data']['cards']['edges']
        assert isinstance(edges_list, list)
        assert len(edges_list) <= count
        if firt_in_list:
            assert get_id(edges_list[0]['node']) == firt_in_list

    @pytest.mark.parametrize("data, status_code", [
        ('mutation {createCard(name: "777"){card {id}}}', 200),  # создание карточки
        ('mutation {removeCard(input:{clientMutationId:"333"}){clientMutationId}}', 200),  # удаление карточки
        ('mutation {changeCard(input:{clientMutationId:"333", name: "333"}){clientMutationId}}', 200),   # изменение карточки
    ])
    def test_mutation_card(self, client, data, status_code):
        r = client.post(url_string(), data=j(query=data), content_type='application/json')
        assert r.status_code == status_code
