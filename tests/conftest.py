import os

import pytest

from app.factory import create_app
from app.extensions import db
from app.models import Card, CardItem


@pytest.fixture(scope='session')
def app():
    os.environ['SETTINGS'] = 'config.Testing'
    the_app = create_app()
    the_app.config['TESTING'] = True
    the_app.config['DEBUG'] = True
    return the_app


@pytest.fixture
def initial_data(app):
    """ fixture для заполнения БД тестовыми значениями"""
    db.create_all()
    db.session.bulk_save_objects([
        Card(name="Card %s" % str(_).rjust(2, '0')) for _ in range(1, 31)
    ], return_defaults=True)
    db.session.bulk_save_objects([
        CardItem(description='fake data', card_id=3),
        CardItem(description='fake data', card_id=3),
        CardItem(description='fake data', card_id=4),
    ], return_defaults=True)
    db.session.commit()
