""" Определение ObjectTypes для schema GraphQl """
import graphene
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType

from . import models


class CardType(SQLAlchemyObjectType):
    class Meta:
        model = models.Card
        interfaces = (relay.Node, )


class CardConnection(relay.Connection):
    class Meta:
        node = CardType


class CardItemInput(graphene.InputObjectType):
    description = graphene.String(required=True)


class CardItemType(SQLAlchemyObjectType):
    class Meta:
        model = models.CardItem
        interfaces = (relay.Node, )


class CardItemConnection(relay.Connection):
    class Meta:
        node = CardItemType
