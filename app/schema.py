""" Определение schema GraphQl """
import graphene
from graphene import Argument, Enum, List, relay
from graphene.types.scalars import Int, String
from graphene_sqlalchemy import SQLAlchemyConnectionField
from graphene_sqlalchemy.utils import EnumValue
from sqlalchemy.inspection import inspect

from .mutations import ChangeCard, CreateCard, RemoveCard
from .types import CardConnection, CardType, CardItemConnection


class SortableCardField(SQLAlchemyConnectionField):
    def _symbol_name(self, column_name, is_asc):
        return column_name + ("_asc" if is_asc else "_desc")

    def sort_argument_for_model(self, model):
        name = model.__name__ + "SortEnum"
        items = []
        default = []
        for column in inspect(model).columns.values():
            if column.__class__.__name__ == 'Label':
                continue
            asc_name = self._symbol_name(column.name, True)
            asc_value = EnumValue(asc_name, column.asc())
            desc_name = self._symbol_name(column.name, False)
            desc_value = EnumValue(desc_name, column.desc())
            if column.primary_key:
                default.append(asc_value)
            items.extend(((asc_name, asc_value), (desc_name, desc_value)))
        # добавляем поля сортирвки по кол-ву действий
        items.extend((
            ('items_count_asc', EnumValue('items_count_asc', model.items_count.asc())),
            ('items_count_desc', EnumValue('items_count_desc', model.items_count.desc())),
        ))
        enum = Enum(name, items)
        return Argument(List(enum), default_value=default)

    def __init__(self, type, *args, **kwargs):
        model = type.Edge.node._type._meta.model
        kwargs.setdefault("sort", self.sort_argument_for_model(model))
        super(SQLAlchemyConnectionField, self).__init__(type, *args, **kwargs)


class Query(graphene.ObjectType):
    node = relay.Node.Field()
    cards = SortableCardField(CardConnection,
                              first=Int(default_value=25),
                              last=Int(default_value=25),
                              name=String(),
                              items=Int())
    card_items = SQLAlchemyConnectionField(CardItemConnection, first=Int(default_value=25), last=Int(default_value=25))

    def resolve_cards(self, info, **kwargs):
        query = CardType.get_query(info)
        if 'name' in kwargs:
            query = query.filter_by(name=kwargs['name'])
        if 'items' in kwargs:
            query = query.filter_by(items_count=kwargs['items'])
        return query.all()


class Mutation(graphene.ObjectType):
    create_card = CreateCard.Field()
    remove_card = RemoveCard.Field()
    change_card = ChangeCard.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
