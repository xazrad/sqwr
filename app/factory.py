import os

from flask import Flask
from flask_graphql import GraphQLView

from .extensions import db, migrate
from .schema import schema


def create_app():
    app = Flask(__name__)
    app.config.from_object(os.environ.get('SETTINGS'))

    db.init_app(app)
    migrate.init_app(app, db)

    app.add_url_rule('/graphql', view_func=GraphQLView.as_view(
        'graphql',
        schema=schema,
        graphiql=True
    ))

    @app.before_first_request
    def initialize_database():
        """ Create all tables """
        db.create_all()

    return app
