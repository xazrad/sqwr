from sqlalchemy.orm import column_property

from app.extensions import db


class CardItem(db.Model):
    """Модель пунктов действий карточек"""
    id = db.Column(db.INTEGER, primary_key=True)
    description = db.Column(db.String(256), nullable=False)
    card_id = db.Column(db.ForeignKey('card.id'), nullable=False, index=True)

    card = db.relationship('Card', backref=db.backref('carditems', cascade='delete,all'))

    def __repr__(self):
        return '<CardItem %r>' % self.id


class Card(db.Model):
    """Модель карточек"""
    id = db.Column(db.INTEGER, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    date_in = db.Column(db.DateTime, default=db.func.now())

    items_count = column_property(
        db.select([db.func.count(CardItem.id)]).where(CardItem.card_id == id)
    )

    def __repr__(self):
        return '<Card %r>' % self.id
