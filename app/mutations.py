""" Определение действий "мутаций" для schema GraphQl """
import graphene

from app.extensions import db

from .models import Card
from .types import CardType, CardItemInput


class CreateCard(graphene.Mutation):
    class Arguments:
        name = graphene.String(required=True)
        items = graphene.List(graphene.NonNull(CardItemInput))

    card = graphene.Field(lambda: CardType)

    def mutate(self, info, name):
        card = Card(name=name)
        db.session.add(card)
        db.session.commit()
        return CreateCard(card=card)


class RemoveCard(graphene.ClientIDMutation):
    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        pk = input['client_mutation_id']
        db.session.query(Card).filter_by(id=pk).delete()
        return pk


class ChangeCard(graphene.ClientIDMutation):
    class Input:
        name = graphene.String(reqired=True)

    @classmethod
    def mutate_and_get_payload(cls, root, info, **input):
        pk = input['client_mutation_id']
        db.session.query(Card).filter_by(id=pk).update({'name': input['name']})
        return pk
