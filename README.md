### Run app in development mode

```
SETTINGS=config.Develop python manage.py run
```

### Run tests

```
pytest
```
