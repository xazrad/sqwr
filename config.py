from pathlib import Path


class Config:
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    BASE_DIR = Path(__file__).parent
    SQLALCHEMY_DATABASE_URI = 'sqlite:////{}/sqlite.db'.format(BASE_DIR)


class Testing(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'  # In RAM


class Develop(Config):
    DEBUG = True


class Staging(Develop):
    pass
